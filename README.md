# Security Policies Team General Project

## Important links

1. [Group page](https://about.gitlab.com/handbook/engineering/development/sec/govern/security-policies/)
1. [Priorities](https://about.gitlab.com/direction/govern/security_policies/#priorities)
1. [Group board](https://gitlab.com/groups/gitlab-org/-/boards/1754674?milestone_title=Started&label_name[]=group%3A%3Asecurity%20policies)
1. [Group metrics](https://about.gitlab.com/handbook/engineering/metrics/sec/govern/security-policies/)


## Slack

1. `#g_srm_security_policies`
1. `#g_srm_security_policies_standup`
